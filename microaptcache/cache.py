#!/usr/bin/env python
#
# Minimal HTTP caching proxy.
#

import BaseHTTPServer
import hashlib
import os
import urllib2
import sys


cache_root = os.getenv('CACHE_DIR', '.cache')


def copyfd(infd, *outfds):
    while True:
        data = infd.read(65536)
        if not data:
            break
        for out in outfds:
            out.write(data)


def cache_path(key, type='data'):
    return os.path.join(cache_root, '%s.%s' % (key, type))


def is_cached(key):
    return os.path.exists(cache_path(key))


def skip_cache(path):
    filename = os.path.basename(path)
    if filename.endswith('.deb'):
        return False
    return True


def serve_from_cache(w, key):
    try:
        with open(cache_path(key, 'ctype')) as fd:
            content_type = fd.read().strip()
    except:
        content_type = 'application/octet-stream'
    w.send_response(200)
    w.send_header('Content-Type', content_type)
    w.send_header('Content-Length', str(os.path.getsize(cache_path(key))))
    w.end_headers()
    with open(cache_path(key)) as fd:
        copyfd(fd, w.wfile)


def passthrough(w, key, req, cache=True):
    try:
        resp = urllib2.urlopen(req)
    except urllib2.URLError, e:
        w.send_response(e.code)
        w.end_headers()
        copyfd(e, w.wfile)
        return
    w.send_response(200)
    w.send_header('Content-Type', resp.headers.get('Content-Type',
                                      'application/octect-stream'))
    if 'Content-Length' in resp.headers:
        w.send_header('Content-Length', resp.headers['Content-Length'])
    w.end_headers()
    if not cache:
        copyfd(resp, w.wfile)
    else:
        with open(cache_path(key, 'ctype'), 'w') as fd:
            fd.write('%s\n' % resp.headers.get('Content-Type',
                                  'application/octect-stream'))
        with open(cache_path(key), 'w') as fd:
            copyfd(resp, fd, w.wfile)


class CacheHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_GET(self):
      m = hashlib.md5()
      m.update(self.path)
      key = m.hexdigest()

      if skip_cache(self.path):
          print "Cache skip: ", self.path
          passthrough(self, key, self.path, cache=False)
      elif is_cached(key):
          print "Cache hit: ", self.path
          serve_from_cache(self, key)
      else:
          print "Cache miss: ", self.path
          passthrough(self, key, self.path)


def main():
    if not os.path.isdir(cache_root):
        os.makedirs(cache_root)
    server_address = ('', int(sys.argv[1]))
    httpd = BaseHTTPServer.HTTPServer(server_address, CacheHandler)
    httpd.serve_forever()


if __name__ == '__main__':
    main()
