#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='microaptcache',
    version='0.1',
    description='Minimal APT short-lived cache',
    author='ale',
    author_email='ale@incal.net',
    url='http://git.autistici.org/ale/microaptcache',
    packages=find_packages(),
    install_requires=[],
    setup_requires=[],
    zip_safe=True,
    entry_points={
        'console_scripts': [
            'microaptcache = microaptcache.cache:main',
        ],
    },
)
